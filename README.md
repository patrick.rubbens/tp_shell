# TP_Shell

## Sujet abordé
Donner brièvement le sujet abordé dans le cours.
> Utiliser une console: ligne de commande.
## Objectifs
Quels sont les éléments du programme de NSI visés, à la fois en terme de contenu et de compétences. 

*Histoire de l’informatique*
>Événements clés de l’histoire de l’informatique.  
>Contenus: Situer dans le temps les principaux événementsde l’histoire de l’informatique et leurs protagonistes.

*Architectures matérielles et systèmes d’exploitation*
>Systèmes d’exploitation :  
>Contenus: Identifier les fonctions d’un système d’exploitation.   
>Capacités attendues: Utiliser les commandes de base en ligne de commande.Gérer les droits et permissions d’accès aux fichiers.  
>Commentaires: Les différences entre systèmes d’exploitation libres et propriétaires sont évoquées.Les élèves utilisent un système d’exploitation libre. Il ne s’agit pas d’une étude théorique des systèmes d’exploitation.
## Pré-requis
Quels sont les éventuels points qui doivent être connus des élèves pour ce cours ?
> On part de 0... Aucun prérequis n'est attendu.
## Préparation
Quels sont les éventuels besoins techniques ou en terme de configuration nécessaires à la partie pratique ?
> Le problème ici sera d'utiliser un "système d’exploitation libre" comme mentionné dans les commentaires du B.O. Notre lycée est équipé de machines tournant exclusivement sur Windows. Reste la solution de VirtualBox...
## Éléments de cours
Donner ici les éléments qui seront abordés dans le cours (on ne vous demande pas d'écrire une leçon complète).
>Pourquoi utiliser les lignes de commandes?  
>Utilisateur vs Administrateur.  
>Quelles sont les commandes les plus utilisées? (et donc les plus utiles à connaitre)  
## Séance pratique
Décrivez ici les manipulations qui devront être réalisées par les élèves pour illustrer les concepts abordés.
>Demander une recherche historique sur les Shells Unix, à situer sur une frise historique dynamique complétée en cours d'année.  
>Manipulations de répertoires et de fichiers.  
## QCM E3C
Quelles questions peuvent être proposées pour le QCM d'épreuves communes de contrôle continu (E3C) en classe de première ?
> [des exemples](http://www.qcmquiz.com/Database/Quiz-Unix-Linux.php)

# Pourquoi utiliser une console et des lignes de commandes?
La console est un moyen de transmettre des ordres à l'ordinateur en utilisant des commandes constituées de lignes de caractères.
Une question légitime que chacun peut se poser : _"Pourquoi faire l'effort d'apprendre à utiliser une console et des lignes de commandes ? l'interface graphique est bien plus accessible, intuitive et je peux tout faire avec."_
C'est en partie vrai, mais en partie seulement, car on ne peut pas TOUT faire et il existe des situations pour lesquelles la ligne de commande est très pratique :
[citation du wiki Debian:](https://wiki.debian.org/fr/Console)
> La console est communément la fameuse fenêtre noire dans laquelle on peut écrire plein de lignes de commandes. Concrètement, il s'agit d'un outil extrêmement puissant et complémentaire aux outils graphiques de votre système.
    La console est souvent à tort appelée "Terminal" ou encore "Shell". Pour faire simple, la console est en fait une combinaison d'un terminal et d'un shell. Mais généralement on les tolère comme des synonymes.
Le shell est un programme exécutable en mode terminal, dont la principale fonction est de permettre à l’utilisateur d’interagir avec le système via un terminal. _Il est parfois appelé interpréteur de commandes_

![exemple du shell bash](images/Bash_screenshot.png)  
By Emx - Wikimedia Foundation, GPL, https://commons.wikimedia.org/w/index.php?curid=509725

-   Lorsque l'interface graphique ne fonctionne plus, la ligne de commande par exemple, peut être la seule méthode pour redémarrer correctement.
-   Les lignes de commande sont souvent plus précises (plus d'options) que l'interface graphique.
-   Pendant son exécution, la ligne de commande vous retourne beaucoup plus d'informations a propos du travail en cours (mode verbeux). Si une application ne démarre pas par exemple, essayez de la démarrer dans une console, elle ne démarrera pas mieux mais vous saurez pourquoi grâce aux commentaires retournés par la console.
-   La ligne de commande nécessite moins de ressources matérielles.
-   Beaucoup de serveurs ne s'administrent qu'en ligne de commande: pas de besoins pour une interface graphique sur un serveur. ( et pourrait être source de dysfonctionnements.)
-   Les commandes sont toujours disponibles, sur n'importe quel ordinateur, ont en gros les mêmes pour n'importe quelle distribution Linux (voire même Windows 10, qui embarque Bash)
-   Donner et/ou recevoir de l'aide est plus simple en lignes de commandes. Il est souvent confus et long d'expliquer où cliquer dans la succession d'écrans d'une interface graphique, alors que le copier/coller de commandes, mêmes nombreuses, en console est toujours très simple et rapide, tout se passe dans la même fenêtre. De plus il est également simple de faire suivre la réponse du système vers celui qui aide une fois de plus par un copier/coller, alors que faire suivre une image graphique est beaucoup plus long et complexe.

# Utilisateur / Administrateur
Il existe aujourd’hui plus d’une trentaine de shells différents, mais deux grandes familles dominent :
- Csh, tcsh : shells orientés administration, avec une syntaxe inspirée du langage C   
- Sh (à l’origine: ash), bsh (Bourne shell), bash (Bourne again shell) : shells orientés utilisateur, majoritaires aujourd’hui.

La plupart des scripts shell sont écrits en sh, ou au moins compatibles sh.

Le shell UNIX standard est sh. **Bash** supplante de plus en plus souvent sh (c’est le cas sur Linux).

Il consiste en un mélange de sh et de quelques fonctions du csh (et d’autres encore...), mais il est 100% compatible sh
En tant qu'utilisateur, vos droits sont limités dans le but de protéger le système.
Vous pouvez lire les fichiers un peu partout mais ne les modifier que dans les sous dossiers de /home/utilisateur.

Root est l'"Administrateur", il a tous les droits, y compris celui de tout casser. Donc, ne soyez root uniquement quand c'est nécessaire et **soyez très prudent quand vous êtes root**.

Lancer le Shell, interpréteur de commandes ( la "console" )

Taper "help" . Un ensemble de commandes s’affiche.
Dans cette liste, vous remarquerez que chaque commande mentionnée est accompagnée d’un [-option]. Il s'agit d'options possibles à assortir à la commande.

![exemple2](images/shell_help.jpg)

Le Shell a un fonctionnement simple. Vous tapez le **nom de la commande** suivi de ses **arguments** , séparés par des espaces.

Par exemple, la commande copy (ou cp ): 
`cp monfichier1.txt monfichier2.txt`

va lancer la commande copy (qui est chargée de copier un fichier) en lui donnant deux arguments (le premier est le nom du fichier source et le deuxième, le nom du fichier de destination).

la commande ls : 
`ls`

va lister tout le contenu du répertoire courant. Si on lui ajoute l'argument '*.txt', on aura alors exclusivement la liste de tous les fichiers .txt du répertoire courant

# Les commandes à connaître :
* **ls** (list) pour lister le contenu du répertoire courant
* **cd** (change directory) + _Nom du répertoire_ pour se rendre dans un répertoire
* **cd ..** permet de remonter d'un niveau dans l'arborescence
* **mkdir** (make directory) + _Nom du répertoire_ pour créer un nouveau répertoire
* **pwd** pour afficher l'arborescence complète vers le répertoire courant
* **touch** + _Nom du fichier_ permet de créer un fichier en le plaçant dans le répertoire courant
* **cp** (copy) _Nom du répertoire parent_ /_Nom du fichier_  _Nom du répertoire de destination_ permet de copier le fichier de son répertoire courant vers un répertoire de destination
* **rm** (remove) _Nom du fichier_ permet de supprimer un fichier
* **mv** (move) _Nom du répertoire parent_/_Nom du fichier_ _Nom du répertoire de destination_ permet de déplacer un fichier de son répertoire courant vers un répertoire de destination ; cette commande permet aussi de renommer un fichier
* **rmdir** (remove directory) permet de supprimer un répertoire mais on ne peut pas supprimer un répertoire s'il n'est pas vide
* **tree** permet d'afficher l'arborescence du répertoire courant
* **cat** permet de visualiser un contenu en utilisant un chemin obtenu en séparant les répertoires rencontrés par des **/** ; on différencie le **chemin absolu** suivi à partir de la racine de la hiérachie
* **ln -s** permet de créer un lien symbolique vers un fichier
* **ls - l** + _fichier_ pour avoir des informations précises sur un fichier
par exemple
``
 -rwxr-xr--    1 user     group     12345 Nov 15 09:19 toto
 ``
le premier - indique qu'il s'agit d'un fichier classique
rwx donnent les droits de lecture r, d'écriture w et d'exécution x pour le propriétaire
r-x indique que les utilisateurs du groupe n'ont pas le droit d'écriture
r-- indique que les autres utilisateurs n'ont que le droit de lecture
lecture (r) correspond à 4
écriture (w) correspond à 2
exécution (x) correspond à 1
rwx vaut 7 (4+2+1), r-x faut 5 (4+1) et r-- vaut 4
* **chmod** _Valeur_ _Nom_du_fichier_ permet de modifier les droits d'utilisation d'un fichier
par exemple chmod 222 prive rend impossible en lecture le fichier prive
et chmod 654 prive le rend à nouveau lisible

### Entrées-sorties et redirections
Tous les processus gèrent une table stockant le nom des différents
fichiers qu’ils utilisent. Chaque index de cette table est appelé un
descripteur de fichiers. Par convention les trois premiers
descripteurs correspondent à :

* **l’entrée standard** - descripteur 0 : si le programme exécuté par le processus a besoin de demander des
   informations à l’utilisateur il les lira dans ce fichier (par défaut
   c’est le terminal en mode lecture (i.e. le clavier)).
* **la sortie standard** - descripteur 1 : si le programme a besoin de donner des
   informations à l’utilisateur il les écrira dans ce fichier (par défaut
   c’est le terminal en mode écriture, i.e. l'écran).
* **la sortie d’erreur** - descripteur 2 : si le programme a besoin d’envoyer un
   message d’erreur à l’utilisateur il l’écrira dans ce fichier (par
   défaut c’est le terminal en mode écriture, i.e. l'écran).
   
Il est possible de rediriger ces entrées-sorties vers n'importe quel
type de fichier au moyen de la syntaxe suivante :

* `< fichier` pour rediriger l'entrée standard depuis le fichier
* `> fichier` pour rediriger l'entrée standard vers le fichier


# Quelques recherches à situer historiquement

Quand sont apparus les premiers systèmes capables d'interpréter des lignes de commandes? Comment devait-on procéder avant?

À partir de quelle(s) années peut-on dater l'apparition de terminaux en mode graphique (et des souris) qui a permis une plus grande diffusion de l'informatique?

# Séance Pratique

1.  Lancer le Shell
2.  Se placer dans le répertoire *Documents* présent dans votre répertoire *prenom.nom*
3.  Créer dans ce répertoire l'arborescence suivante NSI/TP_Shell
4.  Se déplacer à présent dans le répertoire *commun*. Lister les répertoires présents.
5.  Copier le répertoire *medias_telecharges* vers Documents/NSI/TP_Shell
6.  Dans ce répertoire se trouve toutes sortes de médias, musiques, vidéos, photos,... Lister uniquement les fichiers '.mp3'
7.  On souhaite maintenant récupérer cette liste au format '.txt' de sorte à pouvoir les manipuler facilement (_pour créer une pochette par exemple_). Proposer une solution.
8.  Visualiser le contenu du fichier obtenu
9.  Enfin donner les droits en lecture mais pas en écriture sur ce fichier
















